﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaNegocio;
namespace CapaPresentacion
{
    public partial class Form1 : Form
    {

        CN_Materias objCn = new CN_Materias();
        private string idMateria = null;
        private bool Editar = false;
        public Form1()
        {
            InitializeComponent();
        }

        /// Inicio de formulario
        private void Form1_Load(object sender, EventArgs e)
        {
            MostrarMaterias();
            LoadDataToTextBox();
            setBtnGuardarState(false);
            setTextBoxState(false);

        }


        ///Utilidades
        private void MostrarMaterias()
        {
            CN_Materias obj = new CN_Materias();
            dgvMaterias.DataSource = obj.MostrarMat();
        }

        private void LoadDataToTextBox()
        {
            Editar = true;
            txtNombre.Text = dgvMaterias.CurrentRow.Cells["nombre"].Value.ToString();
            txtCredito.Text = dgvMaterias.CurrentRow.Cells["creditos"].Value.ToString();
            txtDescripcion.Text = dgvMaterias.CurrentRow.Cells["descripcion"].Value.ToString();
            idMateria = dgvMaterias.CurrentRow.Cells["Id"].Value.ToString();
        }

        private void setTextBoxState(bool IsEnable) {
            txtNombre.Enabled = IsEnable;
            txtCredito.Enabled = IsEnable;
            txtDescripcion.Enabled = IsEnable;
        
        }

        private void setBtnGuardarState(bool isEnable) {
            btnGuardar.Enabled = isEnable;
        }

        private void cleanTextBox() {
            txtNombre.Clear();
            txtCredito.Clear();
            txtDescripcion.Clear();
            txtBuscar.Clear();
        }

        ///Data Grid View
        private void dgvMaterias_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvMaterias.SelectedCells.Count > 0)
            {
                LoadDataToTextBox();
                setTextBoxState(false);
                btnGuardar.Text = "Guardar";
                setBtnGuardarState(false);
            }
        }

       
        ///Botones
        private void btnGuardar_Click(object sender, EventArgs e)
        {

            if (txtNombre.Text != "" && txtCredito.Text != "" && txtDescripcion.Text != "")
            {
                if (Editar == false)
                {
                    try
                    {
                        objCn.insertarMaterias(txtNombre.Text, txtCredito.Text, txtDescripcion.Text);
                        MessageBox.Show("Se inserto correctamente");
                        MostrarMaterias();
                        setTextBoxState(false);
                        cleanTextBox();
                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show("Ocurrio un error:" + ex);
                    }

                }
                else if (Editar == true)
                {
                    try
                    {
                        objCn.editarMateria(txtNombre.Text, txtCredito.Text, txtDescripcion.Text, idMateria);
                        MessageBox.Show("Se edito correctamente");
                        MostrarMaterias();
                        setTextBoxState(false);
                        cleanTextBox();
                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show("Ocurrio un error:" + ex);
                    }

                }

                label4.Text = "Seleccione una accion";
                btnGuardar.Text = "Guardar";
                setBtnGuardarState(false);

            }
            else {
                MessageBox.Show("No puede dejar ningun campo vacio");
            }
            
        }

        private void BtnEdit_Click(object sender, EventArgs e)
        {
            setTextBoxState(true);
            LoadDataToTextBox();
            MostrarMaterias();
            setBtnGuardarState(true);

            label4.Text = "Editar Materia";
            btnGuardar.Text = "Guardar Cambios";
        }
        private void btnNuevo_Click(object sender, EventArgs e)
        {

            setTextBoxState(true);
            Editar = false;
            MostrarMaterias();
            cleanTextBox();
            setBtnGuardarState(true);

            label4.Text = "Crear nueva materia";
            btnGuardar.Text = "Guardar";
        }

        private void BtnDel_Click(object sender, EventArgs e)
        {

            Console.WriteLine(dgvMaterias.SelectedRows.Count);
            if (dgvMaterias.SelectedRows.Count > 0)
            {
                DialogResult dr = MessageBox.Show("Deseas eliminar esta materia?",
                      "Confirmacion", MessageBoxButtons.YesNo);
                switch (dr)
                {
                    case DialogResult.Yes:
                        idMateria = dgvMaterias.CurrentRow.Cells["Id"].Value.ToString();
                        objCn.eliminarMaterias(idMateria);
                        MessageBox.Show("Materia eliminada correctamente");
                        MostrarMaterias();
                        setTextBoxState(false);
                        break;
                    case DialogResult.No:
                        break;
                }

                cleanTextBox();
            }
            else
            {
                MessageBox.Show("Porfavor seleccione una fila");
            }

        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            if (txtBuscar.Text == "")
            {
                MessageBox.Show("Escriba un termino de busqueda");
            }
            else {
                CN_Materias obj = new CN_Materias();
                dgvMaterias.DataSource = obj.BuscarMat(txtBuscar.Text);
                setTextBoxState(false);

            }
        }
    }
}
