﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDatos;
namespace CapaNegocio
{
    public class CN_Materias
    {
        private CD_Materias objCD = new CD_Materias();

        public DataTable MostrarMat()
        {
            DataTable table = new DataTable();
            table = objCD.Mostrar();
            return table;

        }

        public DataTable BuscarMat(string searchTerm)
        {
            DataTable table = new DataTable();
            table = objCD.Buscar(searchTerm);
            return table;

        }

        public void insertarMaterias(string nombre, string credito, string desc)
        {
            
            objCD.CrearMateria(nombre, Convert.ToInt32(credito), desc);

        }

        public void editarMateria(string nombre, string credito, string desc, string id)
        {
            objCD.Editar(nombre, Convert.ToInt32(credito), desc, Convert.ToInt32(id));
        }

        public void eliminarMaterias(string id)
        {
            objCD.Eliminar(Convert.ToInt32(id));
        }

    }
}
