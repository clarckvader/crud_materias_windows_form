﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDatos
{
    public class CD_Materias
    {
        private CD_Conexion conexion = new CD_Conexion();

        SqlDataReader leer;
        DataTable table = new DataTable();
        SqlCommand comando = new SqlCommand();

        public DataTable Mostrar()
        {
            //Transact SQl
            comando.Connection = conexion.AbrirConexion();

            comando.CommandText = "MostrarMaterias"; //Select * from productos
            comando.CommandType = CommandType.StoredProcedure;
            leer = comando.ExecuteReader();
            table.Load(leer);
            conexion.CerrarConexion();
            return table;
        }

        public void Insertar(string nombre, int creditos, string descripcion)
        {

            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "CrearMateria";
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.AddWithValue("@nombre", nombre);
            comando.Parameters.AddWithValue("@creditos", creditos);
            comando.Parameters.AddWithValue("@descripcion", descripcion);

            comando.ExecuteNonQuery();
            comando.Parameters.Clear();

            conexion.CerrarConexion();

        }
        public void CrearMateria(string nombre, int creditos, string descripcion) {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "CrearMateria";
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.AddWithValue("@nombre", nombre);
            comando.Parameters.AddWithValue("@creditos", creditos);
            comando.Parameters.AddWithValue("@descripcion", descripcion);

            comando.ExecuteNonQuery();
            comando.Parameters.Clear();

            conexion.CerrarConexion();
        }
        public void Editar(string nombre, int creditos, string descripcion, int id)
        {

            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "EditarMateria";
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.AddWithValue("@nombre", nombre);
            comando.Parameters.AddWithValue("@creditos", creditos);
            comando.Parameters.AddWithValue("@descripcion", descripcion);
            comando.Parameters.AddWithValue("@id", id);

            comando.ExecuteNonQuery();
            comando.Parameters.Clear();

            conexion.CerrarConexion();
        }

        public void Eliminar(int id)
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "EliminarMateria";
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.AddWithValue("@id", id);
            comando.ExecuteNonQuery();
            comando.Parameters.Clear();

            conexion.CerrarConexion();

        }

        public DataTable Buscar(string searchTerm) 
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "BuscarMaterias"; //Select * from productos
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.AddWithValue("@search", searchTerm);
            leer = comando.ExecuteReader();
            table.Load(leer);
            conexion.CerrarConexion();
            return table;
        }
    }
}
