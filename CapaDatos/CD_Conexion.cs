﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDatos
{
    class CD_Conexion
    {
        private SqlConnection Conexion = new SqlConnection("Server=PC;Database=dbacademico;User Id=sa;Password=manuel955;");

        public SqlConnection AbrirConexion()
        {
            if (Conexion.State == System.Data.ConnectionState.Closed)
                Conexion.Open();
            return Conexion;

        }

        public SqlConnection CerrarConexion()
        {

            if (Conexion.State == System.Data.ConnectionState.Open)
                Conexion.Close();
            return Conexion;

        }
    }
}
